#!/bin/env python3

from time import sleep

import getpass
import gitlab
import os
import requests
import sys

from http.client import HTTPConnection
from urllib.parse import quote as urlencode, urlsplit
HTTPConnection.debuglevel = 0


class DeployedToDifferentHostException(Exception):
    def __init__(self, ioc, host):
        super(DeployedToDifferentHostException, self).__init__()
        self.ioc = ioc
        self.host = host


    def __str__(self):
        return f"{self.ioc} deployed to a different host: {self.host}"


class AlreadyDeployedException(Exception):
    def __init__(self, ioc):
        super(AlreadyDeployedException, self).__init__()
        self.ioc = ioc


    def __str__(self):
        return f"{self.ioc} already deployed"


class NoHostFoundException(Exception):
    def __init__(self, host):
        super(NoHostFoundException, self).__init__()
        self.host = host


    def __str__(self):
        return f"Host '{self.host}' not found"


class NoIOCFoundException(Exception):
    def __init__(self, ioc):
        super(NoIOCFoundException, self).__init__()
        self.ioc = ioc


    def __str__(self):
        return f"IOC '{self.ioc}' not found"


class HTTPError(Exception):
    def __init__(self, result):
        super(HTTPError, self).__init__()
        self.status_code = result.status_code
        try:
            self.reason = result.json()["description"]
        except:
            self.reason = result.reason


    def __str__(self):
        return f"HTTP Error: {self.status_code} {self.reason}"


class CCCE(object):
    def __init__(self, url="ccce.esss.lu.se"):
        super(CCCE, self).__init__()

        self.session = requests.sessions.Session()
        self.cookiejar = requests.cookies.RequestsCookieJar()
        self.url = url
        self.login()
        self.gitlab = gitlab.Gitlab("https://gitlab.esss.lu.se")
        self.__csentry_cache = {}


    @staticmethod
    def __token_file():
        return os.path.join(os.path.expanduser("~"), ".ccce-api-token")


    def get_gitlab_token(self):
        with open(os.path.join(os.path.expanduser("~"), ".gitlab-api-token"), "rt") as token_file:
            return token_file.readline()


    def __load_token(self):
        with open(self.__token_file(), "rt") as token_file:
            return token_file.readline()


    def __renew_token(self):
        try:
            result = self._post(f"https://{self.url}/api/v1/authentication/renew")
        except HTTPError as e:
            print(e)
            self.__do_login()
            return

        self.__save_token(result["token"])


    def __apply_token(self, token):
        self.access_token = token
        self.cookiejar.set("ce-deploy-auth", token, domain=self.url)


    def __save_token(self, token):
        self.__apply_token(token)
        with open(self.__token_file(), "wt") as token_file:
            os.chmod(self.__token_file(), 0o600)
            return token_file.write(self.access_token)


    def __do_login(self):
        user_name = getpass.getuser()
        data={"userName":user_name, "password":getpass.getpass(f"Password for '{user_name}': ")}
        result = self.session.post(f"https://{self.url}/api/v1/authentication/login", headers={ 'Accept': 'application/json' }, json=data)
        if result.status_code < 200 or result.status_code > 299:
            raise HTTPError(result)
        result_json = result.json()
        self.__save_token(result_json["token"])


    def login(self):
        try:
            self.__apply_token(self.__load_token())
        except Exception as e:
            print(e)
            self.__do_login()
            return

        self.__renew_token()


    def _get(self, url, **kwargs):
        result = self.session.get(url, headers={ 'Accept': 'application/json' }, cookies=self.cookiejar, **kwargs)
        if result.status_code != 200:
            raise HTTPError(result)

        return result.json()


    def _post(self, url, **kwargs):
        result = self.session.post(url, headers={ 'Accept': 'application/json' }, cookies=self.cookiejar, **kwargs)
        if result.status_code != 200 and result.status_code != 201:
            raise HTTPError(result)

        return result.json()


    def _get_gitlab_id(self, project):
        try:
            return int(project)
        except:
            pass

        path = urlsplit(project.strip())[2]
        if path.endswith(".git"):
            path = path[:-4]

        # Strip the "/" in the beginning
        if path.startswith("/"):
            path = path[1:]
        project = self.gitlab.projects.get(urlencode(path))
        return int(project.id)


    def get_naming_id(self, iocname):
        try:
            ioc_id = int(iocname)
        except:
            ioc_id = int(self._get(f"https://{self.url}/api/v1/naming/IOCNamesByName?iocName={iocname.strip()}")[0]["id"])

        return ioc_id


    def get_csentry_id(self, hostname):
        try:
            # It might be a csentry id already
            return int(hostname)
        except:
            pass

        try:
            return self.__csentry_cache[hostname]
        except:
            pass

        params = {"query": f'name:"{hostname}"'}
        result = self._get(f"https://{self.url}/api/v1/hosts", params=params)
        if result["totalCount"] == 0:
            raise NoHostFoundException(hostname)
        assert result["totalCount"] == 1, result

        host_id = int(result["csEntryHosts"][0]["csEntryHost"]["id"])
        self.__csentry_cache[hostname] = host_id

        return host_id


    def _get_ioc_by_id(self, iocid):
        return self._get(f"https://{self.url}/api/v1/iocs/{iocid}")


    def _query_ioc(self, query):
        params = {"query": query}
        result = self._get(f"https://{self.url}/api/v1/iocs", params=params)
        if result["totalCount"] == 0:
            raise NoIOCFoundException(query)
        assert result["totalCount"] == 1

        return result["iocList"][0]


    def get_ioc(self, query):
        try:
            iocid = int(query)
        except ValueError:
            return self._query_ioc(query)

        return self._get_ioc_by_id(iocid)


    def get_commits(self, ioc):
        """
        [
          {
            "reference": "0.2.3",
            "shortReference": "0.2.3",
            "description": "",
            "commitDate": null
          },
          {
            "reference": "f6174dc953a03ddce3969214874bf044e1fc5852",
            "shortReference": "f6174dc9",
            "description": "Initial commit\n\nPorted from https://gitlab.esss.lu.se/ioc/e3-ioc-operation\n",
            "commitDate": "2022-01-25T15:33:07.000+00:00"
          }
        ]
        """
        try:
            repo_id = ioc["gitProjectId"]
        except:
            ioc = self.get_ioc(ioc)
            repo_id = ioc["gitProjectId"]

        params = {"projectId": repo_id}
        return self._get(f"https://{self.url}/api/v1/gitHelper/tagsAndCommitIds", params=params)


    def register_ioc(self, ioc, repo):
        ioc_id = self.get_naming_id(ioc)

        data = {"externalNameId":ioc_id, "gitProjectId":self._get_gitlab_id(repo)}
        return self._post(f"https://{self.url}/api/v1/iocs", json=data)


    def deploy_ioc(self, ioc, host, version, comment = None):
        ioc = self.get_ioc(ioc)
        ioc_id = ioc["id"]
        ioc_name = ioc["namingName"]
        host_id = self.get_csentry_id(host)

        if version is None:
            version = self.get_commits(ioc)[0]["reference"]

        if ioc.get("activeDeployment"):
            deployed_to = ioc["activeDeployment"]["host"]["externalHostId"]
            if host_id != deployed_to:
                raise DeployedToDifferentHostException(ioc_name, deployed_to)
            deployed_version = ioc["sourceVersion"]
            if version == deployed_version:
                raise AlreadyDeployedException(ioc_name)

        data={"hostCSEntryId":host_id, "sourceVersion":version}
        if comment:
            data["comment"] = comment
        print(f"Deploying {ioc_name} ({ioc_id}) to {host} ({host_id}) with version {version}")

        result = self._post(f"https://{self.url}/api/v1/iocs/{ioc_id}/deployment_job", json=data)
        print(result)
        deployment_id = result["id"]
        awx_id = result["awxJobId"]
        while True:
            sleep(15)
            result = self._get(f"https://{self.url}/api/v1/deployments/jobs/{awx_id}")
            result = result["status"]
            print(result)
            if result == "successful":
                break
        result = self._get(f"https://{self.url}/api/v1/deployments/{deployment_id}")
        print(result)


    def undeploy_ioc(self, ioc, comment = None):
        ioc = self.get_ioc(ioc)
        ioc_id = ioc["id"]
        ioc_name = ioc["namingName"]

        data={"comment":comment}
        print(f"Undeploying {ioc_name}")
        try:
            result = self._post(f"https://{self.url}/api/v1/iocs/{ioc_id}/undeployment_job", json=data)
        except HTTPError as error:
            if error.status_code == 422:
                return
            raise
        print(result)
        undeployment_id = result["id"]
        awx_id = result["awxJobId"]
        while True:
            sleep(15)
            result = self._get(f"https://{self.url}/api/v1/deployments/jobs/{awx_id}")
            result = result["status"]
            print(result)
            if result == "successful":
                break
        result = self._get(f"https://{self.url}/api/v1/deployments/{undeployment_id}")
        print(result)


    def __start_stop_ioc(self, ioc, cmd):
        ioc = self.get_ioc(ioc)
        ioc_id = ioc["id"]
        ioc_name = ioc["namingName"]

        print(f"{cmd.capitalize()}ing {ioc_name}")
        result = self._post(f"https://{self.url}/api/v1/iocs/{ioc_id}/{cmd}_job")
        print(result)
        command_id = result["id"]
        awx_id = result["awxCommandId"]
        while True:
            sleep(15)
            result = self._get(f"https://{self.url}/api/v1/deployments/commands/jobs/{awx_id}")
            result = result["status"]
            print(result)
            if result == "successful":
                break
        result = self._get(f"https://{self.url}/api/v1/deployments/commands/{command_id}")
        print(result)


    def start_ioc(self, ioc):
        self.__start_stop_ioc(ioc, "start")


    def stop_ioc(self, ioc):
        self.__start_stop_ioc(ioc, "stop")



def iocs(ioclist_file):
    ccce = CCCE()

    with open(ioclist_file, "rt") as ioclist:
        for iocname in ioclist:
            if iocname.startswith("#"):
                continue
            iocname = iocname.split(",")[0]
            iocname = iocname.strip()
            naming_id = ccce.get_naming_id(iocname)
            print(f"{iocname}, {naming_id}")
            print(ccce.get_ioc(iocname))


def register_ioc(args):
    ccce = CCCE()

    iocrepolist_file = args.ioclist_file
    with open(iocrepolist_file, "rt") as ioclist:
        for line in ioclist:
            if line.startswith("#"):
                print(f"Ignoring {line}")
                continue
            try:
                ioc_name, ioc_id, repo_name = line.split(",")
                ioc_name = ioc_id
            except ValueError:
                ioc_name, repo_name = line.split(",")
            result = ccce.get_ioc(ioc_name)
            if result is None:
                result = ccce.register_ioc(ioc_name, repo_name)
                print(result)


def deploy_ioc(args):
    ccce = CCCE()

    iochostlist_file = args.ioclist_file
    with open(iochostlist_file, "rt") as ioclist:
        for line in ioclist:
            if line.startswith("#"):
                print(f"Ignoring {line}")
                continue

            version = None
            try:
                iocname, ioc_id, hostname, version = line.split(",")
            except ValueError:
                try:
                    iocname, hostname, version = line.split(",")
                except ValueError:
                    iocname, hostname = line.split(",")
            iocname = iocname.strip()
            hostname = hostname.strip()
            if version:
                version = version.strip()

            try:
                ccce.deploy_ioc(iocname, hostname, version, comment=args.comment)
            except (AlreadyDeployedException, DeployedToDifferentHostException) as e:
                print(e)


def start_ioc(args):
    ccce = CCCE()

    ioclist_file = args.ioclist_file
    with open(ioclist_file, "rt") as ioclist:
        for line in ioclist:
            if line.startswith("#"):
                print(f"Ignoring {line}")
                continue

            iocname = line.split(",")[0].strip()
            ccce.start_ioc(iocname)


def stop_ioc(args):
    ccce = CCCE()

    ioclist_file = args.ioclist_file
    with open(ioclist_file, "rt") as ioclist:
        for line in ioclist:
            if line.startswith("#"):
                print(f"Ignoring {line}")
                continue

            iocname = line.split(",")[0].strip()
            ccce.stop_ioc(iocname)


def undeploy_ioc(args):
    ccce = CCCE()

    ioclist_file = args.ioclist_file
    with open(ioclist_file, "rt") as ioclist:
        for line in ioclist:
            if line.startswith("#"):
                print(f"Ignoring {line}")
                continue

            iocname = line.split(",")[0]
            ccce.undeploy_ioc(iocname)


def main():
    import argparse
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(title="commands", dest="command")

    register_parser = subparsers.add_parser("register", help="Register IOCs")
    register_parser.set_defaults(func=register_ioc)

    deploy_parser = subparsers.add_parser("deploy", help="Deploy IOCs")
    deploy_parser.set_defaults(func=deploy_ioc)
    deploy_parser.add_argument(
                               "--comment",
                               help="deployment comment",
                               type=str
                              )

    undeploy_parser = subparsers.add_parser("undeploy", help="Undeploy IOCs")
    undeploy_parser.set_defaults(func=undeploy_ioc)

    start_parser = subparsers.add_parser("start", help="Start IOCs")
    start_parser.set_defaults(func=start_ioc)

    stop_parser = subparsers.add_parser("stop", help="Stop IOCs")
    stop_parser.set_defaults(func=stop_ioc)

    parser.add_argument(
                        "ioclist_file",
                        help="ioclist file",
                        type=str
                       )

    args = parser.parse_args()
    args.func(args)


if __name__ == "__main__":
    main()
